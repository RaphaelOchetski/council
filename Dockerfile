FROM gradle:7-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:11
EXPOSE 8080
COPY --from=build  /home/gradle/src/build/libs/*.jar /app/council-app.jar
ENTRYPOINT ["java", "-jar", "/app/council-app.jar"]