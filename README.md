# Pauta

O objetivo do seriço de pauta é replicar um ambiente de uma assembleia. Será possível criar pautas e realizar votações sobre o assunto abordado.

Tecnologias utilizadas:

- Java 11;
- Spring Boot 2.5.4
- Docker
- Mongo
- Kafka

Para testar o projeto, é necessário ter o Docker instalado.
Realize o clone do projeto. Com um terminal vá até a raíz do mesmo e execute:

`docker-compose up -d`

Após a subida com sucesso dos containers, execute:

`./gradlew bootRun` (Linux)

`gradlew bootRun` (Windows)

Depois, acesse http://localhost:8080/swagger-ui/index.html para acessar o Swagger do serviço.


OBS: Na branch `dockerizing-app` o serviço também está mapeado no docker-compose, sendo não necessário o uso do gradle. 
A ideia do uso do gradlew é facilitar a visualização dos logs da aplicação.
