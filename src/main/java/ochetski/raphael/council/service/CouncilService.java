package ochetski.raphael.council.service;

import ochetski.raphael.council.api.request.council.CouncilRequest;
import ochetski.raphael.council.exception.CouncilNotFoundException;
import ochetski.raphael.council.model.Council;
import ochetski.raphael.council.repository.CouncilRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CouncilService {

    private final CouncilRepository councilRepository;

    public CouncilService(CouncilRepository repository) {
        this.councilRepository = repository;
    }

    public Mono<Council> createCouncil(CouncilRequest request) {
        return councilRepository.save(new Council(request.getProposal()));
    }

    public Mono<Council> findCouncilById(String councilId) {
        return councilRepository.findById(councilId)
                .switchIfEmpty(Mono.error(new CouncilNotFoundException()));
    }

    public Mono<Council> updateCouncil(Council council) {
        return councilRepository.save(council);
    }

    public Flux<Council> findCouncils() {
        return councilRepository.findAll();
    }
}
