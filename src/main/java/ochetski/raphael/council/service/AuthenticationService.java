package ochetski.raphael.council.service;

import ochetski.raphael.council.client.UserInfoClient;
import ochetski.raphael.council.enums.UserInfoStatus;
import ochetski.raphael.council.exception.UserUnableToVoteException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class AuthenticationService {

    private final UserInfoClient userInfoClient;

    public AuthenticationService(UserInfoClient userInfoClient) {
        this.userInfoClient = userInfoClient;
    }

    public Mono<Void> authenticate(String document, String password) {
        //abstracting user authentication
        return userInfoClient.userVotePossibilityVerification(document)
                .filter(userStatusResponse -> userStatusResponse.getStatus() == UserInfoStatus.ABLE_TO_VOTE)
                .switchIfEmpty(Mono.error(new UserUnableToVoteException()))
                .then();
    }

}
