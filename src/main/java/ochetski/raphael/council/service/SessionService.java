package ochetski.raphael.council.service;

import ochetski.raphael.council.api.request.session.SessionCreationRequest;
import ochetski.raphael.council.api.request.session.UserIdRequest;
import ochetski.raphael.council.enums.CouncilStatus;
import ochetski.raphael.council.enums.VotingOption;
import ochetski.raphael.council.exception.SessionAlreadyClosedException;
import ochetski.raphael.council.exception.SessionNotFoundException;
import ochetski.raphael.council.exception.UserAlreadyVotedException;
import ochetski.raphael.council.model.Council;
import ochetski.raphael.council.model.Session;
import ochetski.raphael.council.model.Vote;
import ochetski.raphael.council.producer.CouncilNotificationProducer;
import ochetski.raphael.council.producer.event.CouncilEvent;
import ochetski.raphael.council.repository.SessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;

@Service
public class SessionService {

    private final Logger log = LoggerFactory.getLogger(SessionService.class);

    private final SessionRepository sessionRepository;
    private final CouncilService councilService;
    private final AuthenticationService authenticationService;
    private final VoteService voteService;
    private final CouncilNotificationProducer producer;
    private Integer defaultSessionDuration;

    public SessionService(SessionRepository repository,
                          CouncilService councilService,
                          AuthenticationService authenticationService,
                          VoteService voteService,
                          CouncilNotificationProducer producer,
                          @Value("${session.default-min}") Integer defaultSessionDuration) {
        this.sessionRepository = repository;
        this.councilService = councilService;
        this.authenticationService = authenticationService;
        this.voteService = voteService;
        this.producer = producer;
        this.defaultSessionDuration = defaultSessionDuration;
    }

    public Mono<Session> createSession(SessionCreationRequest request) {
        return councilService.findCouncilById(request.getCouncilId())
                .map(council -> council.setStatus(CouncilStatus.UNDER_VOTE))
                .flatMap(councilService::updateCouncil)
                .flatMap(council -> sessionRepository.save(new Session(request.getCouncilId(), getSessionDuration(request))))
                .doOnNext(session -> closingSessionSchedule(session.getId(), session.getDuration()));
    }

    public Mono<String> generateVotingId(String sessionId, UserIdRequest request) {
        return authenticationService.authenticate(request.getDocument(), request.getPassword())
                .then(findSessionById(sessionId))
                .map(session -> session.getId() + request.getDocument()); //abstracting id complex creation
    }

    public Mono<Void> vote(VotingOption option, String sessionId, String userId) {
        return validateSession(sessionId, userId)
                .map(session -> option == VotingOption.YES ? session.addYesVote() : session.addNoVotes())
                .flatMap(this::updateSession)
                .flatMap(session -> voteService.saveVote(new Vote(sessionId, userId)));
    }

    public Flux<Session> findAllSessions() {
        return sessionRepository.findAll();
    }

    public Mono<Session> findSessionById(String sessionId) {
        return sessionRepository.findById(sessionId)
                .switchIfEmpty(Mono.error(new SessionNotFoundException()));
    }

    private Mono<Session> updateSession(Session session) {
        return sessionRepository.save(session);
    }

    protected void closeSession(String sessionId) {
        log.info("Encerrando sessão de ID {}", sessionId);
        sessionRepository.findById(sessionId)
                .map(Session::closeSession)
                .flatMap(this::updateSession)
                .doOnNext(session -> councilService.findCouncilById(session.getCouncilId())
                        .map(council -> council.setCouncilResult(session))
                        .flatMap(councilService::updateCouncil)
                        .doOnSuccess(council -> producer.sendCouncilResult(buildCouncilEvent(council)))
                        .subscribe())
                .doOnSuccess(session -> log.info("Sessão {} encerrada com sucesso! Sim: [{}] X Não: [{}]", session.getId(), session.getYesVotes(), session.getNoVotes()))
                .subscribe();
    }

    private Mono<Session> validateSession(String sessionId, String userId) {
        return voteService.userAlreadyVoted(sessionId, userId)
                .filter(exists -> !exists)
                .switchIfEmpty(Mono.error(new UserAlreadyVotedException()))
                .then(findSessionById(sessionId)
                        .filter(session -> !session.isClosed())
                        .switchIfEmpty(Mono.error(new SessionAlreadyClosedException())));
    }

    private Integer getSessionDuration(SessionCreationRequest request) {
        return isNull(request.getDuration()) || request.getDuration().equals(0) ? defaultSessionDuration : request.getDuration();
    }

    private void closingSessionSchedule(String sessionId, Integer duration) {
        ScheduledExecutorService service = Executors.newScheduledThreadPool(5);
        service.schedule(() -> closeSession(sessionId), duration, TimeUnit.MINUTES);
    }

    private CouncilEvent buildCouncilEvent(Council council) {
        return CouncilEvent.builder()
                .councilId(council.getId())
                .proposal(council.getProposal())
                .status(council.getStatus())
                .build();
    }
}
