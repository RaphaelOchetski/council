package ochetski.raphael.council.service;

import ochetski.raphael.council.model.Vote;
import ochetski.raphael.council.repository.VoteRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class VoteService {

    private final VoteRepository voteRepository;

    public VoteService(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    public Mono<Void> saveVote(Vote vote) {
        return voteRepository.save(vote)
                .then();
    }

    public Mono<Boolean> userAlreadyVoted(String sessionId, String userId) {
        return voteRepository.existsBySessionIdAndUserId(sessionId, userId);
    }
}
