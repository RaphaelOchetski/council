package ochetski.raphael.council.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "session")
public class Session {

    @Id
    String id;
    String councilId;
    Integer yesVotes;
    Integer noVotes;
    Boolean closed;
    Integer duration;
    LocalDateTime sessionOpenedAt;
    LocalDateTime sessionClosedAt;

    public Session(String councilId, Integer duration) {
        this.councilId = councilId;
        this.duration = duration;
        this.yesVotes = 0;
        this.noVotes = 0;
        this.closed = Boolean.FALSE;
        this.sessionOpenedAt = LocalDateTime.now();
    }

    private Session() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCouncilId() {
        return councilId;
    }

    public Integer getYesVotes() {
        return yesVotes;
    }

    public Session addYesVote() {
        this.yesVotes += 1;
        return this;
    }

    public Integer getNoVotes() {
        return noVotes;
    }

    public Session addNoVotes() {
        this.noVotes += 1;
        return this;
    }

    public Boolean isClosed() {
        return closed;
    }

    public Session closeSession() {
        this.sessionClosedAt = LocalDateTime.now();
        this.closed = Boolean.TRUE;
        return this;

    }

    public Integer getDuration() {
        return duration;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public Boolean getClosed() {
        return closed;
    }

    public LocalDateTime getSessionOpenedAt() {
        return sessionOpenedAt;
    }

    public LocalDateTime getSessionClosedAt() {
        return sessionClosedAt;
    }
}
