package ochetski.raphael.council.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "vote")
public class Vote {

    @Id
    private String id;
    private String sessionId;
    private String userId;

    public Vote(String sessionId, String userId) {
        this.sessionId = sessionId;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getUserId() {
        return userId;
    }

}
