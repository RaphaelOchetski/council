package ochetski.raphael.council.model;

import ochetski.raphael.council.enums.CouncilStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "council")
public class Council {

    @Id
    String id;
    String proposal;
    CouncilStatus status;

    public Council(String proposal) {
        this.proposal = proposal;
        this.status = CouncilStatus.WAITING_SESSION;
    }


    private Council() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public CouncilStatus getStatus() {
        return status;
    }

    public Council setStatus(CouncilStatus status) {
        this.status = status;
        return this;
    }

    public Council setCouncilResult(Session session) {
        this.status = session.getYesVotes() > session.getNoVotes() ? CouncilStatus.APPROVED :
                session.getYesVotes().equals(session.getNoVotes()) ? CouncilStatus.UNDECIDED : CouncilStatus.REPROVED;
        return this;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Council council;

        private Builder() {
            council = new Council();
        }

        public Builder id(String id) {
            council.setId(id);
            return this;
        }

        public Builder proposal(String proposal) {
            council.setProposal(proposal);
            return this;
        }

        public Builder status(CouncilStatus status) {
            council.setStatus(status);
            return this;
        }

        public Council build() {
            return council;
        }
    }
}
