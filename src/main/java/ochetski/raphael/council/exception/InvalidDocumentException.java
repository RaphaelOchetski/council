package ochetski.raphael.council.exception;

import org.springframework.http.HttpStatus;

public class InvalidDocumentException extends DefaultException{

    public InvalidDocumentException() {
        super("Documento inválido.", HttpStatus.BAD_REQUEST);
    }
}
