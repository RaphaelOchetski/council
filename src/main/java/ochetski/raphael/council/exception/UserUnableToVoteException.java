package ochetski.raphael.council.exception;

import org.springframework.http.HttpStatus;

public class UserUnableToVoteException extends DefaultException {

    public UserUnableToVoteException() {
        super("Associado não autorizado a votar.", HttpStatus.UNAUTHORIZED);
    }
}
