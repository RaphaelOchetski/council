package ochetski.raphael.council.exception;

import org.springframework.http.HttpStatus;

public class UserAlreadyVotedException extends DefaultException{

    public UserAlreadyVotedException() {
        super("Associado já votou nesta sessão.", HttpStatus.UNAUTHORIZED);
    }
}
