package ochetski.raphael.council.exception;

import org.springframework.http.HttpStatus;

public class CouncilNotFoundException extends DefaultException {

    public CouncilNotFoundException() {
        super("Pauta não existente.", HttpStatus.NOT_FOUND);
    }
}
