package ochetski.raphael.council.exception;

import org.springframework.http.HttpStatus;

public class SessionNotFoundException extends DefaultException {

    public SessionNotFoundException() {
        super("Sessão não existente", HttpStatus.NOT_FOUND);
    }
}
