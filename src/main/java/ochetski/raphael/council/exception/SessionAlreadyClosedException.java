package ochetski.raphael.council.exception;

import org.springframework.http.HttpStatus;

public class SessionAlreadyClosedException extends DefaultException{

    public SessionAlreadyClosedException() {
        super("Sessão de votação já encerrada.", HttpStatus.EXPECTATION_FAILED);
    }
}
