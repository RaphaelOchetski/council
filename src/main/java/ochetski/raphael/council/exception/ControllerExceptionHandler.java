package ochetski.raphael.council.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler({DefaultException.class})
    public ResponseEntity<ErrorMessage> defaultExceptionHandler(DefaultException e) {
        return new ResponseEntity<>(new ErrorMessage(e.getError()), e.getHttpStatus());
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<ErrorMessage> validationExceptionHandler(WebExchangeBindException e) {
        return new ResponseEntity<>(new ErrorMessage("Falta um parâmetro"), HttpStatus.BAD_REQUEST);
    }

}
