package ochetski.raphael.council.exception;

import org.springframework.http.HttpStatus;

public abstract class DefaultException extends RuntimeException {

    private String error;
    private HttpStatus httpStatus;

    public DefaultException(String error, HttpStatus httpStatus) {
        this.error = error;
        this.httpStatus = httpStatus;
    }

    public String getError() {
        return error;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
