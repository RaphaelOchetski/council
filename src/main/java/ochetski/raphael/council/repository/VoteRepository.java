package ochetski.raphael.council.repository;

import ochetski.raphael.council.model.Vote;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface VoteRepository extends ReactiveCrudRepository<Vote, String> {

    Mono<Boolean> existsBySessionIdAndUserId(String sessionId, String userId);

}
