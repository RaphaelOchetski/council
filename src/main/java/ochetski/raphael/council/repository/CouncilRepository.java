package ochetski.raphael.council.repository;

import ochetski.raphael.council.model.Council;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CouncilRepository extends ReactiveCrudRepository<Council, String> {
}
