package ochetski.raphael.council.producer.event;

import ochetski.raphael.council.enums.CouncilStatus;

public class CouncilEvent {

    private String councilId;
    private String proposal;
    private CouncilStatus status;

    public String getCouncilId() {
        return councilId;
    }

    public void setCouncilId(String councilId) {
        this.councilId = councilId;
    }

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public CouncilStatus getStatus() {
        return status;
    }

    public void setStatus(CouncilStatus status) {
        this.status = status;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private CouncilEvent councilEvent;

        private Builder() {
            councilEvent = new CouncilEvent();
        }

        public Builder councilId(String councilId) {
            councilEvent.setCouncilId(councilId);
            return this;
        }

        public Builder proposal(String proposal) {
            councilEvent.setProposal(proposal);
            return this;
        }

        public Builder status(CouncilStatus status) {
            councilEvent.setStatus(status);
            return this;
        }

        public CouncilEvent build() {
            return councilEvent;
        }
    }
}
