package ochetski.raphael.council.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ochetski.raphael.council.producer.event.CouncilEvent;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderRecord;

@Component
public class CouncilNotificationProducer {

    private static final Logger log = LoggerFactory.getLogger(CouncilNotificationProducer.class);
    private static final Integer CORRELATION_METADATA = 1;

    private final KafkaSender kafkaSender;
    private final String topic;
    private final ObjectMapper objectMapper;

    public CouncilNotificationProducer(KafkaSender kafkaSender, @Value("${spring.kafka.topic.council-result}") String topic, ObjectMapper objectMapper) {
        this.kafkaSender = kafkaSender;
        this.topic = topic;
        this.objectMapper = objectMapper;
    }

    public void sendCouncilResult(CouncilEvent event) {
        kafkaSender.send(buildMessage(event))
                .doOnSubscribe(subscription -> log.info("Resultado da pauta {} enviado com sucesso. Status: {}", event.getCouncilId(), event.getStatus()))
                .doOnError(throwable -> log.error("Erro ao enviar evento de resultado da pauta {} para o kafka. Erro: [{}]", event.getCouncilId(), throwable))
                .subscribe();
    }

    private Mono<SenderRecord<String, String, Integer>> buildMessage(CouncilEvent event) {
        return Mono.just(SenderRecord.create(new ProducerRecord<>(topic, toBinary(event)), CORRELATION_METADATA));
    }

    private String toBinary(Object processMessage) {
        try {
            return objectMapper.writeValueAsString(processMessage);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
