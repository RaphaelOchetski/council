package ochetski.raphael.council.config;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    private String servers;

    @Bean
    KafkaSender kafkaSender() {
        Map<String, Object> producerPros = new HashMap<>();
        producerPros.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerPros.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerPros.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
        SenderOptions<Integer, String> producerOptions = SenderOptions.create(producerPros);
        return KafkaSender.create(producerOptions);
    }

}
