package ochetski.raphael.council.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Value("${spring.kafka.topic.council-result}")
    private String councilResultTopic;

    @Bean
    public NewTopic topic1() {
        return TopicBuilder.name(councilResultTopic).build();
    }

}
