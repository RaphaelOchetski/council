package ochetski.raphael.council.api.response.session;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class SessionResponse {

    private String id;
    private String councilId;
    private Boolean closed;
    private Integer duration;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    private LocalDateTime sessionOpenedAt;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    private LocalDateTime sessionClosedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCouncilId() {
        return councilId;
    }

    public void setCouncilId(String councilId) {
        this.councilId = councilId;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public LocalDateTime getSessionOpenedAt() {
        return sessionOpenedAt;
    }

    public void setSessionOpenedAt(LocalDateTime sessionOpenedAt) {
        this.sessionOpenedAt = sessionOpenedAt;
    }

    public LocalDateTime getSessionClosedAt() {
        return sessionClosedAt;
    }

    public void setSessionClosedAt(LocalDateTime sessionClosedAt) {
        this.sessionClosedAt = sessionClosedAt;
    }
}
