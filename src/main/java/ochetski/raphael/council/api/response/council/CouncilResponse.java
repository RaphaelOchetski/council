package ochetski.raphael.council.api.response.council;

import io.swagger.annotations.ApiModelProperty;
import ochetski.raphael.council.enums.CouncilStatus;

public class CouncilResponse {

    @ApiModelProperty(value = "ID da pauta")
    String id;
    @ApiModelProperty(value = "Proposta da pauta")
    String proposal;
    @ApiModelProperty(value = "Status atual da pauta")
    CouncilStatus status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public CouncilStatus getStatus() {
        return status;
    }

    public void setStatus(CouncilStatus status) {
        this.status = status;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private CouncilResponse councilResponse;

        private Builder() {
            councilResponse = new CouncilResponse();
        }

        public Builder id(String id) {
            councilResponse.setId(id);
            return this;
        }

        public Builder proposal(String proposal) {
            councilResponse.setProposal(proposal);
            return this;
        }

        public Builder status(CouncilStatus status) {
            councilResponse.setStatus(status);
            return this;
        }

        public CouncilResponse build() {
            return councilResponse;
        }
    }
}
