package ochetski.raphael.council.api.response.session;

import io.swagger.annotations.ApiModelProperty;

public class SessionCreationResponse {

    @ApiModelProperty(value = "ID da sessão")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
