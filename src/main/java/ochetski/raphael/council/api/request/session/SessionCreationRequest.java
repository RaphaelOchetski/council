package ochetski.raphael.council.api.request.session;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class SessionCreationRequest {

    @ApiModelProperty(value = "ID da pauta", required = true)
    @NotBlank(message = "ID da pauta é obrigatório.")
    private String councilId;

    @ApiModelProperty(value = "Tempo da sessão em minutos")
    @Min(1)
    private Integer duration;

    public String getCouncilId() {
        return councilId;
    }

    public void setCouncilId(String councilId) {
        this.councilId = councilId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private SessionCreationRequest sessionCreationRequest;

        private Builder() {
            sessionCreationRequest = new SessionCreationRequest();
        }

        public Builder councilId(String councilId) {
            sessionCreationRequest.setCouncilId(councilId);
            return this;
        }

        public Builder duration(Integer duration) {
            sessionCreationRequest.setDuration(duration);
            return this;
        }

        public SessionCreationRequest build() {
            return sessionCreationRequest;
        }
    }
}
