package ochetski.raphael.council.api.request.council;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;

public class CouncilRequest {

    @ApiModelProperty(value = "Proposta da pauta", required = true)
    @NotBlank(message = "Proposta da pauta é obrigatório")
    String proposal;

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private CouncilRequest councilRequest;

        private Builder() {
            councilRequest = new CouncilRequest();
        }

        public Builder proposal(String proposal) {
            councilRequest.setProposal(proposal);
            return this;
        }

        public CouncilRequest build() {
            return councilRequest;
        }
    }
}
