package ochetski.raphael.council.api.request.session;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;

public class UserIdRequest {

    @ApiModelProperty(value = "Documento do assiociado", required = true)
    @NotBlank(message = "Documento do associado é obrigatório")
    private String document;
    @ApiModelProperty(value = "Senha do assiociado", required = true)
    @NotBlank(message = "Senha do associado é obrigatória")
    private String password;

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private UserIdRequest userIdRequest;

        private Builder() {
            userIdRequest = new UserIdRequest();
        }

        public Builder document(String document) {
            userIdRequest.setDocument(document);
            return this;
        }

        public Builder password(String password) {
            userIdRequest.setPassword(password);
            return this;
        }

        public UserIdRequest build() {
            return userIdRequest;
        }
    }
}
