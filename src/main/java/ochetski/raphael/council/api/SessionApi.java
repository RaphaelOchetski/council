package ochetski.raphael.council.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import ochetski.raphael.council.api.request.session.SessionCreationRequest;
import ochetski.raphael.council.api.request.session.UserIdRequest;
import ochetski.raphael.council.api.response.session.SessionCreationResponse;
import ochetski.raphael.council.api.response.session.SessionResponse;
import ochetski.raphael.council.enums.VotingOption;
import ochetski.raphael.council.service.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/sessions")
public class SessionApi {

    private final Logger log = LoggerFactory.getLogger(SessionApi.class);
    private final SessionService sessionService;
    private final ObjectMapper objectMapper;

    public SessionApi(SessionService sessionService, ObjectMapper objectMapper) {
        this.sessionService = sessionService;
        this.objectMapper = objectMapper;
    }

    @PostMapping
    @ApiOperation(value = "Cria uma sessão de votação", response = SessionCreationResponse.class)
    public Mono<ResponseEntity<SessionCreationResponse>> createSession(@Valid @RequestBody SessionCreationRequest request) {
        log.info("Iniciando criação de sessão de votação para pauta {}", request.getCouncilId());
        return sessionService.createSession(request)
                .map(session -> objectMapper.convertValue(session, SessionCreationResponse.class))
                .doOnSuccess(response -> log.info("Sessão iniciada com sucesso. ID {}", response.getId()))
                .map(response -> new ResponseEntity<>(response, HttpStatus.CREATED));
    }

    @PostMapping("/{sessionId}/user-id")
    @ApiOperation(value = "Gera um id para votar em uma sessão", response = String.class)
    public Mono<String> createSessionId(@PathVariable String sessionId,
                                        @Valid @RequestBody UserIdRequest request) {
        log.info("Gerando ID de votação para o associado {} e sessão {}", request.getDocument(), sessionId);
        return sessionService.generateVotingId(sessionId, request)
                .doOnSuccess(generatedId -> log.info("ID para o associado {} gerado com sucesso.", request.getDocument()));

    }

    @PostMapping("/{sessionId}/vote")
    @ApiOperation(value = "Realiza votação na sessão")
    public Mono<Void> vote(@RequestParam @ApiParam(value = "Opcão de voto", required = true) VotingOption option,
                           @RequestParam @ApiParam(value = "ID do usuário", required = true) String userId,
                           @PathVariable String sessionId) {
        log.info("Contabilizando voto do usuário {} para a sessão {}", userId, sessionId);
        return sessionService.vote(option, sessionId, userId)
                .doOnSuccess(aVoid -> log.info("Voto do usuário {} na sessão {} contabilizado com sucesso", userId, sessionId));
    }

    @GetMapping("/{sessionId}")
    @ApiOperation(value = "Retorna sessão pelo id", response = SessionResponse.class)
    public Mono<SessionResponse> findSessionById(@PathVariable String sessionId) {
        log.info("Buscando sessão de id {}", sessionId);
        return sessionService.findSessionById(sessionId)
                .map(session -> objectMapper.convertValue(session, SessionResponse.class));
    }

    @GetMapping
    @ApiOperation(value = "Lista todas as sessões", response = SessionResponse.class)

    public Flux<SessionResponse> findAllSessions() {
        log.info("Buscando todas as sessões.");
        return sessionService.findAllSessions()
                .map(session -> objectMapper.convertValue(session, SessionResponse.class));
    }

}
