package ochetski.raphael.council.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import ochetski.raphael.council.api.request.council.CouncilRequest;
import ochetski.raphael.council.api.response.council.CouncilResponse;
import ochetski.raphael.council.service.CouncilService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/councils")
public class CouncilApi {

    private final Logger log = LoggerFactory.getLogger(CouncilApi.class);
    private final CouncilService councilService;
    private final ObjectMapper objectMapper;

    public CouncilApi(CouncilService service, ObjectMapper objectMapper) {
        this.councilService = service;
        this.objectMapper = objectMapper;
    }

    @PostMapping
    @ApiOperation(value = "Cria uma pauta.", response = CouncilResponse.class)
    public Mono<ResponseEntity<CouncilResponse>> createCouncil(@Valid @RequestBody CouncilRequest request) {
        log.info("Iniciando criação da pauta {}", request.getProposal());
        return councilService.createCouncil(request)
                .map(council -> objectMapper.convertValue(council, CouncilResponse.class))
                .doOnSuccess(response -> log.info("Pauta criada com sucesso. ID {}", response.getId()))
                .map(response -> new ResponseEntity<>(response, HttpStatus.CREATED));
    }

    @GetMapping("/{councilId}")
    @ApiOperation(value = "Retorna pauta pelo id", response = CouncilResponse.class)
    public Mono<CouncilResponse> findCouncilById(@PathVariable String councilId) {
        log.info("Buscando pauta de id {}", councilId);
        return councilService.findCouncilById(councilId)
                .map(council -> objectMapper.convertValue(council, CouncilResponse.class));
    }

    @GetMapping
    @ApiOperation(value = "Retorna todas as pautas", response = CouncilResponse[].class)
    public Flux<CouncilResponse> findAllCouncils() {
        log.info("Buscando todas as pautas.");
        return councilService.findCouncils()
                .map(council -> objectMapper.convertValue(council, CouncilResponse.class));
    }

}
