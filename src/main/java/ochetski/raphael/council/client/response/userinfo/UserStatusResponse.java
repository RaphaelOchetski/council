package ochetski.raphael.council.client.response.userinfo;

import ochetski.raphael.council.enums.UserInfoStatus;

public class UserStatusResponse {

    private UserInfoStatus status;

    public UserInfoStatus getStatus() {
        return status;
    }

    public void setStatus(UserInfoStatus status) {
        this.status = status;
    }
}
