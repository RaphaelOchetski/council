package ochetski.raphael.council.client;

import ochetski.raphael.council.client.response.userinfo.UserStatusResponse;
import ochetski.raphael.council.exception.CouncilNotFoundException;
import ochetski.raphael.council.exception.InvalidDocumentException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

@Component
public class UserInfoClient {

    private final WebClient webClient;
    private final String userInfoUrl;

    public UserInfoClient(WebClient webClient, @Value("${user-info.url}") String userInfoUrl) {
        this.webClient = webClient;
        this.userInfoUrl = userInfoUrl;
    }

    public Mono<UserStatusResponse> userVotePossibilityVerification(String document) {
        return webClient.get()
                .uri(buildUserVotePossibilityVerificationUrl(document))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> Mono.error(new InvalidDocumentException()))
                .bodyToMono(UserStatusResponse.class);
    }

    private UriComponentsBuilder baseUrl() {
        return UriComponentsBuilder.fromUriString(userInfoUrl).pathSegment("users");
    }

    private String buildUserVotePossibilityVerificationUrl(String document) {
        return baseUrl().pathSegment(document).toUriString();
    }

}
