package ochetski.raphael.council.enums;

public enum CouncilStatus {

    UNDER_VOTE,
    APPROVED,
    REPROVED,
    UNDECIDED,
    WAITING_SESSION

}
