package ochetski.raphael.council.enums;

public enum UserInfoStatus {

    ABLE_TO_VOTE,
    UNABLE_TO_VOTE

}
