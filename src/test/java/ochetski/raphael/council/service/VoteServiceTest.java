package ochetski.raphael.council.service;

import ochetski.raphael.council.mocks.VoteMock;
import ochetski.raphael.council.model.Vote;
import ochetski.raphael.council.repository.VoteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VoteServiceTest {

    private VoteRepository voteRepository;
    private VoteService voteService;

    @BeforeEach
    public void setup() {
        this.voteRepository = mock(VoteRepository.class);
        this.voteService = new VoteService(voteRepository);
    }

    @Test
    public void saveVoteWithSuccess() {
        when(voteRepository.save(any(Vote.class))).thenReturn(Mono.just(VoteMock.create()));

        StepVerifier.create(voteService.saveVote(VoteMock.create()))
                .verifyComplete();
    }

    @Test
    public void userAlreadyVotedShouldReturnWithTrueValue() {
        when(voteRepository.existsBySessionIdAndUserId(anyString(), anyString())).thenReturn(Mono.just(Boolean.TRUE));

        StepVerifier.create(voteService.userAlreadyVoted("sessionId", "userId"))
                .expectNextMatches(aBoolean -> aBoolean.equals(Boolean.TRUE))
                .verifyComplete();
    }

}
