package ochetski.raphael.council.service;

import ochetski.raphael.council.client.UserInfoClient;
import ochetski.raphael.council.enums.UserInfoStatus;
import ochetski.raphael.council.exception.UserUnableToVoteException;
import ochetski.raphael.council.mocks.UserStatusResponseMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthenticationServiceTest {

    private UserInfoClient userInfoClient;
    private AuthenticationService authenticationService;

    @BeforeEach
    public void setup() {
        this.userInfoClient = mock(UserInfoClient.class);
        this.authenticationService = new AuthenticationService(userInfoClient);
    }

    @Test
    public void authenticateUserWithSuccess() {
        when(userInfoClient.userVotePossibilityVerification(anyString()))
                .thenReturn(Mono.just(UserStatusResponseMock.create(UserInfoStatus.ABLE_TO_VOTE)));

        StepVerifier.create(authenticationService.authenticate("document", "password"))
                .verifyComplete();
    }

    @Test
    public void userNotAbleToVoteShouldThrowUserUnableToVoteException() {
        when(userInfoClient.userVotePossibilityVerification(anyString()))
                .thenReturn(Mono.just(UserStatusResponseMock.create(UserInfoStatus.UNABLE_TO_VOTE)));

        StepVerifier.create(authenticationService.authenticate("document", "passwword"))
                .expectError(UserUnableToVoteException.class)
                .verify();
    }

}
