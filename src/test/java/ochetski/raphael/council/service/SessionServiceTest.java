package ochetski.raphael.council.service;

import ochetski.raphael.council.enums.VotingOption;
import ochetski.raphael.council.exception.SessionAlreadyClosedException;
import ochetski.raphael.council.exception.UserAlreadyVotedException;
import ochetski.raphael.council.mocks.CouncilMock;
import ochetski.raphael.council.mocks.SessionCreationRequestMock;
import ochetski.raphael.council.mocks.SessionMock;
import ochetski.raphael.council.mocks.UserIdRequestMock;
import ochetski.raphael.council.model.Council;
import ochetski.raphael.council.model.Session;
import ochetski.raphael.council.model.Vote;
import ochetski.raphael.council.producer.CouncilNotificationProducer;
import ochetski.raphael.council.producer.event.CouncilEvent;
import ochetski.raphael.council.repository.SessionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SessionServiceTest {

    private SessionRepository sessionRepository;
    private CouncilService councilService;
    private AuthenticationService authenticationService;
    private VoteService voteService;
    private CouncilNotificationProducer producer;
    private Integer defaultSessionDuration;
    private SessionService sessionService;

    @BeforeEach
    public void setup() {
        this.sessionRepository = mock(SessionRepository.class);
        this.councilService = mock(CouncilService.class);
        this.authenticationService = mock(AuthenticationService.class);
        this.voteService = mock(VoteService.class);
        this.producer = mock(CouncilNotificationProducer.class);
        this.sessionService = new SessionService(sessionRepository, councilService, authenticationService, voteService, producer, defaultSessionDuration);
        ReflectionTestUtils.setField(sessionService, "defaultSessionDuration", 0);
    }

    @Test
    public void createSessionWithSuccess() {
        when(councilService.findCouncilById(anyString())).thenReturn(Mono.just(CouncilMock.create()));
        when(councilService.updateCouncil(any(Council.class))).thenReturn(Mono.just(CouncilMock.create()));
        when(sessionRepository.save(any(Session.class))).thenReturn(Mono.just(SessionMock.create(1, Boolean.FALSE)));

        StepVerifier.create(sessionService.createSession(SessionCreationRequestMock.create()))
                .expectNextMatches(session -> session.getCouncilId().equals("councilId"))
                .verifyComplete();

        verify(councilService).findCouncilById(anyString());
        verify(councilService).updateCouncil(any(Council.class));
        verify(sessionRepository).save(any(Session.class));
    }

    @Test
    public void closeSessionWithSuccess() {
        when(sessionRepository.findById(anyString())).thenReturn(Mono.just(SessionMock.create(1, Boolean.FALSE)));
        when(sessionRepository.save(any(Session.class))).thenReturn(Mono.just(SessionMock.create(1, Boolean.FALSE)));
        when(councilService.findCouncilById(anyString())).thenReturn(Mono.just(CouncilMock.create()));
        when(councilService.updateCouncil(any(Council.class))).thenReturn(Mono.just(CouncilMock.create()));
        doNothing().when(producer).sendCouncilResult(any(CouncilEvent.class));

        sessionService.closeSession("1");

        verify(sessionRepository).findById(anyString());
        verify(sessionRepository).save(any(Session.class));
        verify(councilService).findCouncilById(anyString());
        verify(councilService).updateCouncil(any(Council.class));
        verify(producer).sendCouncilResult(any(CouncilEvent.class));
    }

    @Test
    public void generateVotingIdWithSuccess() {
        when(authenticationService.authenticate(anyString(), anyString())).thenReturn(Mono.empty());
        when(sessionRepository.findById(anyString())).thenReturn(Mono.just(SessionMock.create(1, Boolean.FALSE)));

        StepVerifier.create(sessionService.generateVotingId("1", UserIdRequestMock.create()))
                .expectNextMatches(id -> id.equals("sessionId1234567890"))
                .verifyComplete();

        verify(authenticationService).authenticate(anyString(), anyString());
        verify(sessionRepository).findById(anyString());
    }

    @Test
    public void voteWithSuccess() {
        when(sessionRepository.findById(anyString())).thenReturn(Mono.just(SessionMock.create(1, Boolean.FALSE)));
        when(voteService.userAlreadyVoted(anyString(), anyString())).thenReturn(Mono.just(Boolean.FALSE));
        when(sessionRepository.save(any(Session.class))).thenReturn(Mono.just(SessionMock.create(1, Boolean.FALSE)));
        when(voteService.saveVote(any(Vote.class))).thenReturn(Mono.empty());

        StepVerifier.create(sessionService.vote(VotingOption.YES, "sessionId", "userId"))
                .verifyComplete();

        verify(sessionRepository).findById(anyString());
        verify(voteService).userAlreadyVoted(anyString(), anyString());
        verify(sessionRepository).save(any(Session.class));
        verify(voteService).saveVote(any(Vote.class));
    }

    @Test
    public void voteInClosedSessionShouldThrowSessionAlreadyClosedException() {
        when(sessionRepository.findById(anyString())).thenReturn(Mono.just(SessionMock.create(1, Boolean.TRUE)));
        when(voteService.userAlreadyVoted(anyString(), anyString())).thenReturn(Mono.just(Boolean.FALSE));

        StepVerifier.create(sessionService.vote(VotingOption.YES, "sessionId", "userId"))
                .expectError(SessionAlreadyClosedException.class)
                .verify();
    }

    @Test
    public void duplicatedVoteShouldThrowUserAlreadyVotedException() {
        when(sessionRepository.findById(anyString())).thenReturn(Mono.just(SessionMock.create(1, Boolean.FALSE)));
        when(voteService.userAlreadyVoted(anyString(), anyString())).thenReturn(Mono.just(Boolean.TRUE));

        StepVerifier.create(sessionService.vote(VotingOption.YES, "sessionId", "userId"))
                .expectError(UserAlreadyVotedException.class)
                .verify();
    }

}
