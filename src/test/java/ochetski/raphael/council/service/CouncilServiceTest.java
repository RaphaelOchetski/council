package ochetski.raphael.council.service;

import ochetski.raphael.council.exception.CouncilNotFoundException;
import ochetski.raphael.council.mocks.CouncilMock;
import ochetski.raphael.council.mocks.CouncilRequestMock;
import ochetski.raphael.council.model.Council;
import ochetski.raphael.council.repository.CouncilRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CouncilServiceTest {

    private CouncilRepository councilRepository;
    private CouncilService councilService;

    @BeforeEach
    public void setup() {
        this.councilRepository = mock(CouncilRepository.class);
        this.councilService = new CouncilService(councilRepository);
    }

    @Test
    public void createCouncilWithSuccess() {
        when(councilRepository.save(any(Council.class))).thenReturn(Mono.just(CouncilMock.create()));

        StepVerifier.create(councilService.createCouncil(CouncilRequestMock.create()))
                .expectNextMatches(council -> council.getProposal().equals("proposal"))
                .verifyComplete();
    }

    @Test
    public void nonExistentCouncilShouldThrowCouncilNotFoundException() {
        when(councilRepository.findById(anyString())).thenReturn(Mono.empty());

        StepVerifier.create(councilService.findCouncilById("councilId"))
                .expectError(CouncilNotFoundException.class)
                .verify();
    }

    @Test
    public void findCouncilsWithSuccess() {
        when(councilRepository.findAll()).thenReturn(Flux.just(CouncilMock.create(), CouncilMock.create()));

        StepVerifier.create(councilService.findCouncils())
                .expectNextCount(2)
                .verifyComplete();
    }

    @Test
    public void updateWithSuccess() {
        when(councilRepository.save(any(Council.class))).thenReturn(Mono.just(CouncilMock.create()));

        StepVerifier.create(councilService.updateCouncil(CouncilMock.create()))
                .expectNextMatches(council -> council.getProposal().equals("proposal"))
                .verifyComplete();
    }

}
