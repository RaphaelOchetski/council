package ochetski.raphael.council.mocks;

import ochetski.raphael.council.client.response.userinfo.UserStatusResponse;
import ochetski.raphael.council.enums.UserInfoStatus;

public class UserStatusResponseMock {

    public static UserStatusResponse create(UserInfoStatus status) {
        UserStatusResponse userStatusResponse = new UserStatusResponse();
        userStatusResponse.setStatus(status);
        return userStatusResponse;
    }

}
