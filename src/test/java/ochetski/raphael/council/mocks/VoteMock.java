package ochetski.raphael.council.mocks;

import ochetski.raphael.council.model.Vote;

public class VoteMock {

    public static Vote create() {
        return new Vote("sessionId", "userId");
    }

}
