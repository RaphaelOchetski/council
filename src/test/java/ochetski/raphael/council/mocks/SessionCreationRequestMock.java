package ochetski.raphael.council.mocks;

import ochetski.raphael.council.api.request.session.SessionCreationRequest;

public class SessionCreationRequestMock {

    public static SessionCreationRequest create() {
        return SessionCreationRequest.builder()
                .councilId("councilId")
                .duration(1)
                .build();
    }

}
