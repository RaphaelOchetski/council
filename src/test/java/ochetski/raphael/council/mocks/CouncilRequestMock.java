package ochetski.raphael.council.mocks;

import ochetski.raphael.council.api.request.council.CouncilRequest;

public class CouncilRequestMock {

    public static CouncilRequest create() {
        return CouncilRequest.builder()
                .proposal("proposal")
                .build();
    }

}
