package ochetski.raphael.council.mocks;

import ochetski.raphael.council.model.Session;

public class SessionMock {

    public static Session create(Integer duration, Boolean closed) {
        Session session = new Session("councilId", duration);
        session.setClosed(closed);
        session.setId("sessionId");
        return session;
    }

}
