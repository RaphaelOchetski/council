package ochetski.raphael.council.mocks;

import ochetski.raphael.council.api.request.session.UserIdRequest;

public class UserIdRequestMock {

    public static UserIdRequest create() {
        return UserIdRequest.builder()
                .document("1234567890")
                .password("password")
                .build();
    }

}
