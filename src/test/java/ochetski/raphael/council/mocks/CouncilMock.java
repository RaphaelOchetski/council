package ochetski.raphael.council.mocks;

import ochetski.raphael.council.enums.CouncilStatus;
import ochetski.raphael.council.model.Council;

public class CouncilMock {

    public static Council create() {
        return Council.builder()
                .id("councilId")
                .proposal("proposal")
                .status(CouncilStatus.WAITING_SESSION)
                .build();
    }

}
