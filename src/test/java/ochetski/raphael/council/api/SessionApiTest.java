package ochetski.raphael.council.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ochetski.raphael.council.api.request.session.SessionCreationRequest;
import ochetski.raphael.council.api.request.session.UserIdRequest;
import ochetski.raphael.council.mocks.SessionCreationRequestMock;
import ochetski.raphael.council.mocks.SessionMock;
import ochetski.raphael.council.mocks.UserIdRequestMock;
import ochetski.raphael.council.service.SessionService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SessionApiTest {

    private SessionService sessionService;
    private ObjectMapper objectMapper;

    private WebTestClient webTestClient;

    @BeforeAll
    public void setup() {
        this.sessionService = mock(SessionService.class);
        this.objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).findAndRegisterModules();

        webTestClient = WebTestClient.bindToController(new SessionApi(sessionService, objectMapper))
                .configureClient()
                .baseUrl("/v1/sessions")
                .defaultHeader("Content-Type", "application/json")
                .build();
    }

    @Test
    public void createSessionWithSuccess() throws IOException {
        when(sessionService.createSession(any(SessionCreationRequest.class)))
                .thenReturn(Mono.just(SessionMock.create(1, Boolean.FALSE)));

        String json = new String(Files.readAllBytes(Paths.get("src/test/resources/json/sessionResponse.json")));

        webTestClient.post()
                .body(BodyInserters.fromValue(SessionCreationRequestMock.create()))
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody()
                .json(json);
    }

    @Test
    public void createSessionIdWithSuccess() {
        when(sessionService.generateVotingId(anyString(), any(UserIdRequest.class))).thenReturn(Mono.just("id"));

        webTestClient.post()
                .uri("/123/user-id")
                .body(BodyInserters.fromValue(UserIdRequestMock.create()))
                .exchange()
                .expectStatus()
                .is2xxSuccessful();
    }

    @Test
    public void voteShouldReturnSuccess() {
        when(sessionService.vote(any(), anyString(), anyString())).thenReturn(Mono.empty());

        webTestClient.post()
                .uri("/123/vote?option=YES&userId=1")
                .exchange()
                .expectStatus()
                .is2xxSuccessful();
    }

}
