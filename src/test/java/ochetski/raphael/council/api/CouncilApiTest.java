package ochetski.raphael.council.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import ochetski.raphael.council.api.request.council.CouncilRequest;
import ochetski.raphael.council.mocks.CouncilMock;
import ochetski.raphael.council.mocks.CouncilRequestMock;
import ochetski.raphael.council.service.CouncilService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CouncilApiTest {

    private CouncilService councilService;
    private ObjectMapper objectMapper;

    private WebTestClient webTestClient;

    @BeforeAll
    public void setup() {
        this.councilService = mock(CouncilService.class);
        this.objectMapper = spy(ObjectMapper.class);

        webTestClient = WebTestClient.bindToController(new CouncilApi(councilService, objectMapper))
                .configureClient()
                .baseUrl("/v1/councils")
                .defaultHeader("Content-Type", "application/json")
                .build();
    }

    @Test
    public void createCouncilWithSuccess() throws IOException {
        when(councilService.createCouncil(any(CouncilRequest.class))).thenReturn(Mono.just(CouncilMock.create()));

        String json = new String(Files.readAllBytes(Paths.get("src/test/resources/json/councilResponse.json")));

        webTestClient.post()
                .body(BodyInserters.fromValue(CouncilRequestMock.create()))
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody()
                .json(json);
    }

    @Test
    public void findUsingIdShouldReturnSuccess() throws IOException {
        when(councilService.findCouncilById(anyString())).thenReturn(Mono.just(CouncilMock.create()));

        String json = new String(Files.readAllBytes(Paths.get("src/test/resources/json/councilResponse.json")));

        webTestClient.get()
                .uri("/councilId")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .json(json);
    }

    @Test
    public void findAllCouncilsWithSuccess() throws IOException {
        when(councilService.findCouncils()).thenReturn(Flux.just(CouncilMock.create()));

        String json = new String(Files.readAllBytes(Paths.get("src/test/resources/json/councilArrayResponse.json")));

        webTestClient.get()
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .json(json);
    }

}
